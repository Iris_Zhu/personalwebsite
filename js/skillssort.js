	$(document).ready(function() {
		$('ul#filter a').click(function() {
			$('ul#filter .current').removeClass('current');
			$(this).parent().addClass('current');
			var filterVal = $(this).text();			
			if(filterVal == 'all') {
				$('ul#skillsname li.hidden').fadeIn('slow');
			}else{
				$('ul#skillsname li').each(function() {
					if(!$(this).hasClass(filterVal)) {
						$(this).fadeOut('normal').addClass('hidden');
					} else {
						$(this).fadeIn('slow').removeClass('hidden');
					}
				});
			}
			
			return false;
		});
	});