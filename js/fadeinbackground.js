$(document).ready(function(){
    $('.fadein img:gt(0)').hide();
    setInterval(function(){
      $('.fadein :first-child').fadeOut(1200)
         .next('img').fadeIn(1200)
         .end().appendTo('.fadein');},1200);
});