Personal Website          
                                                                                                          
•	Designed a responsive website from scratch for both frontend(JavaScript) and backend(PHP)

•	Set up Linux, Apache, MySQL and PHP(LAMP) environment. Created an online database to realize membership administration with MySQL. Used PHP for server side to manage MySQL databases 

•	Worked with Bootstrap framework for mobile-first design

•	Built user interactive features with jQuery, JavaScript and Google APIs libraries

•	Used Chrome/Firefox Developer Tool for coding and debugging. Tested website under various systems (Linux/Windows/OSX) and different versions of browsers(Safari/Chrome/IE/Firefox)